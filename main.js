// todos
// 1 - go through each cell and checks if it's a block or free and if it is then ignore that path
// 2 - make a path maker and finder function
// 3 - ...
// hints
// you can check each row and col till you get to the destination

let canvas = document.querySelector(".canvas");
(function makeCanvas() {
  let block, player, col, row;
  for (let i = 0; i < 171; i++) {
    block = Math.ceil(Math.random() * 10) > 9 && i !== 0 ? true : false;
    player = i === 0 ? true : false;
    col = (i + 1) % 19 == 0 ? 19 : i % 19;
    row = (i - col) % 9;
    canvas.appendChild(makeCell(block, player, i + 1, row, col));
  }
})();

function makeCell(block, player, id, row, col) {
  let cell = document.createElement("div");
  let span = document.createElement("span");
  cell.setAttribute("onclick", "setDest(this,event);");
  cell.setAttribute("key", id);
  cell.setAttribute("row", row);
  cell.setAttribute("col", col);
  cell.appendChild(span);
  cell.classList.add("cell");
  if (block) cell.classList.add("block");
  if (player) {
    cell.appendChild(document.createElement("div")).classList.add("player");
  }
  return cell;
}

function setDest(elem, e) {
  if (elem.classList.contains("block"))
    return alert("You can't set your destination there!");
  elem.classList.add("destenation");
  let row = parseInt(elem.getAttribute("row"));
  let col = parseInt(elem.getAttribute("col"));
  let distance = row + col;
  setDistance(elem.getAttribute("key"), row, col, distance);
}

function setDistance(id, dRow, dCol, d) {
  let BreakException = {};
  try {
    let distance;
    let row = 0;
    let col = 0;
    dRow = parseInt(dRow);
    dCol = parseInt(dCol);
    id = parseInt(id);
    document.querySelectorAll(".cell").forEach((elem, idx) => {
      if (elem.classList.contains("block")) return;

      let span = elem.lastChild;
      if (id === idx + 1) return;
      row = elem.getAttribute("row") * 1;
      col = elem.getAttribute("col") * 1;
      if (dRow < row || dCol < col) return;
      distance = (row + col) * 1;
      span.innerHTML = `${distance}`;

      if (distance <= 1) elem.classList.add("far");
      if (distance <= 2 && distance > 1) elem.classList.add("notFar");
      if (distance <= 5 && distance > 2) elem.classList.add("close");
      if (distance <= 8 && distance > 5) elem.classList.add("closer");
      if (distance <= 8 && distance > 5) elem.classList.add("closest");
      if (distance > 8) elem.classList.add("superClose");
    });
  } catch (e) {
    if (e !== BreakException) throw e;
  }
}
